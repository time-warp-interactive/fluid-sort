import { AbstractBottle } from "./bottles/bottle-generic.ts";
import { NormalBottle } from "./bottles/normal-bottle.ts"
export class Logic {
    public colors = 0;
    public bottles: AbstractBottle[] = [];
    public bottleCount = 2;
    private history = new Array<[number, number]>()
    
    constructor() {
        this.bottles.push(new NormalBottle(4, 0));
        this.bottles.push(new NormalBottle(4, 0));
        this.addBottle();
    }

    viewAll(): Array<Array<number>>{
        const a = [];
        for(const bottle of this.bottles){
            a.push(bottle.view());
        }
        return a;
    }

    atob(BottleA: number, BottleB: number): boolean{
        const a = this.bottles[BottleA];
        const b = this.bottles[BottleB];

        if(BottleA == BottleB){
            return false;
        }

        if(a.removalColor() != b.removalColor() && b.removalColor() != 0){
            return false;
        }

        if(b.filled() || a.isPerfect()){
            return false;
        }

        const {color, quantity} = a.remove();
        const remaining = b.add(color, quantity);
        a.return(color, remaining);

        this.history.push([BottleA, BottleB]);
        return true;
    }

    addBottle(): void {
        this.colors++;
        this.bottleCount++;
        this.bottles.push(new NormalBottle(4, this.colors));
    }

    addEmptyBottle(): void {
        this.bottleCount++;
        this.bottles.push(new NormalBottle(4, 0));
    }

    undo(): void {
        const [BottleA, BottleB] = this.history.pop() || [-1, -1];
        if(BottleA == -1){
            return;
        }
        this.bottles[BottleA].undo();
        this.bottles[BottleB].undo();
    }

    shuffle(iterations: number, maxBottles: number){
        for(let i = 0; i < iterations; i++){
            const move = Math.floor(Math.random() * 2);
            switch(move){
                case 0:
                    if(this.bottleCount <= maxBottles){
                        this.addBottle();
                        break;
                    }
                    /* deno-lint-ignore no-fallthrough */
                case 1:
                    this.shufflePour();
                    break;
            }
        }
        for(const bottle of this.bottles){
            bottle.resetHistory();
        }
    }

    shufflePour(){
        const a = this.bottles[Math.floor(Math.random() * this.bottles.length)];
        const b = this.bottles[Math.floor(Math.random() * this.bottles.length)];
        const {color, quantity} = a.shuffleRemove(Infinity);
        const m = Math.floor(Math.random() * (quantity + 1))
        const remaining = b.shuffleAdd(color, m);
        a.shuffleReturn(color, quantity - (m - remaining));
    }

    hasWon(): boolean {
        for(const bottle of this.bottles){
            if(!bottle.isPerfect()){
                 return false;
            }
        }
        return true;
    }
}