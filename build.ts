import { emptyDir, walk } from "https://deno.land/std/fs/mod.ts";
import { Language, minify } from "https://deno.land/x/minifier/mod.ts";
import { JSZip } from "https://deno.land/x/jszip@0.9.0/mod.ts";

const HTMLTest = /.*\.html/;
const CSSTest = /.*\.css/;

{
  const path = /static\//;
  await emptyDir("./public");
  for await (const entry of walk("./static")){
    if(entry.isDirectory){
      continue;
    }
    const writePath = entry.path.replace(path, "public/");
    if(HTMLTest.test(entry.name)){
      const code = await Deno.readTextFile(entry.path);
      await Deno.writeTextFile(writePath, minify(Language.HTML, code));
    }
    if(CSSTest.test(entry.name)){
      const code = await Deno.readTextFile(entry.path);
      await Deno.writeTextFile(writePath, minify(Language.CSS, code));
    }
  }
  const { files, diagnostics } = await Deno.emit(
    `./src/client.ts`,
    {
      bundle: "esm"
    }
  )

  if (diagnostics.length) {
    // there is something that impacted the emit
    console.warn(Deno.formatDiagnostics(diagnostics));
  }
  Deno.writeTextFile("./public/client.js", minify(Language.JS ,files["deno:///bundle.js"]));
}

console.log("Bundling is done!")

{
  const zip = new JSZip();
  const path = /public\//;
  for await (const entry of walk("./public")){
      if(entry.isDirectory){
        continue;
      }
      const archivePath = entry.path.replace(path, "");
      zip.addFile(archivePath, await Deno.readFile(entry.path));
  }
  const zippedFile = await zip.generateAsync({
    compression: "DEFLATE",
    type: "uint8array"
  });
  await Deno.writeFile("./public/archive.zip", zippedFile);
}
console.log("Zip is done!");