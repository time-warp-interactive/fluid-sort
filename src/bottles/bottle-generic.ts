export type removed = {
    color: number,
    quantity: number,
}
export abstract class AbstractBottle {
    protected slots: number[];
    protected history: Array<number[]> = Array<number[]>();
    public readonly slotMax: number;
    private perfect = false;
    constructor(slots: number, fillWith: number){
        this.slots = new Array(slots).fill(fillWith);
        this.slotMax = slots-1;
        this.checkPerfect();
    }

    view(): number[]{
        return this.slots;
    }

    protected checkPerfect(): boolean {
        const c = this.slots[0];
        for(const slot of this.slots){
            if(slot != c){
                this.perfect = false;
                return this.perfect;
            }
        }
        this.perfect = true;
        return this.perfect;
    }

    isPerfect(): boolean {
        return this.perfect;
    }
    
    abstract filled(): boolean
    abstract removalColor(): number
    abstract remove(max?: number): removed
    abstract add(color: number, max: number): number
    abstract return(color: number, max: number): void
    abstract shuffleRemove(max?: number): removed
    abstract shuffleAdd(color: number, max: number): number
    abstract shuffleReturn(color: number, max: number): void
    abstract resetHistory(): void
    abstract undo(): void
}