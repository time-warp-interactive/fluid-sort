import { AbstractBottle, removed } from "./bottle-generic.ts";

export class NormalBottle extends AbstractBottle {
    filled(): boolean {
        if(this.slots[this.slotMax] != 0){
            return true;
        } else {
            return false;
        }
    }

    removalColor(): number {
        let color = 0;
        for(let i = this.slotMax; i >= 0; i--){
            if(this.slots[i] != 0){
                color = this.slots[i];
                break;
            }
        }
        return color;
    }

    remove(max?: number): removed {
        this.history.push([...this.slots]);
        let left = max || Infinity;
        let color = 0;
        let quantity = 0;
        let i = this.slotMax;
        while(i >= 0){
            const t = this.slots[i];
            if(color == 0 && t != 0){
                color = t;
            }
            if(t == color){
                if(t != 0){
                    quantity++;
                    left--;
                    this.slots[i] = 0;
                    if(left == 0){
                        break;
                    }
                }
            } else {
                break;
            }
            i--;
        }
        return {color, quantity};
    }

    add(color: number, max: number): number {
        this.history.push([...this.slots]);
        let s = 0;
        let left = max;
        while(s<=this.slotMax){
            if(this.slots[s] == 0 && left > 0){
                this.slots[s] = color;
                left--;
            }
            s++;
        }
        this.checkPerfect();
        return left;
    }

    return(color: number, max: number): void {
        let s = 0;
        let left = max;
        while(s<=this.slotMax){
            if(this.slots[s] == 0 && left > 0){
                this.slots[s] = color;
                left--;
            }
            s++;
        }
        this.checkPerfect();
    }

    shuffleRemove(max?: number): removed {
        return this.remove(max);
    }
    shuffleAdd(color: number, max: number): number {
        return this.add(color, max);
    }
    shuffleReturn(color: number, max: number): void {
        return this.return(color, max);
    }

    resetHistory(): void {
        this.history = new Array<number[]>();
    }

    undo(): void {
        const last = this.history.pop();
        if(last){
            this.slots = last;
            this.checkPerfect();
        }
    }
}