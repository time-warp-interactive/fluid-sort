/// <reference lib="esNext" />
/// <reference lib="dom" />
/// <reference lib="webworker.importScripts" />
/// <reference lib="ScriptHost" />
/// <reference lib="dom.iterable" />
/// <reference no-default-lib="true"/>
import { Logic } from "./game-logic.ts";

const colors = [
    "LightGray",
    "#f94144",
    "#f3722c",
    "#f8961e",
    "#f9c74f",
    "#90be6d",
    "#43aa8b",
    "#577590",
    "#7400b8",
    "#003566",
]
export class GameGUI {
    base: HTMLDivElement;
    game: Logic | null = null;

    bottles: HTMLDivElement[] = [];
    queued: number | null = null;
    won = true;
    constructor(interactionWindow: HTMLDivElement) {
        this.base = interactionWindow;
        this.guiGenerator();
    }

    guiGenerator() {
        {
            const newGame = document.createElement("div");
            newGame.innerText = "New Game!";
            newGame.id = "NewGame";
            newGame.addEventListener("click", ()=>{this.newGame();});
            this.base.appendChild(newGame);
        }
        {
            const undo = document.createElement("div");
            undo.innerText = "↩️";
            undo.id = "undo";
            undo.addEventListener("click", ()=>{this.undo();});
            this.base.appendChild(undo);
        }
    }

    undo() {
        if(!this.game){
            return;
        }
        this.game.undo();
        this.updateBottles();
        this.hasWon();
    }

    clearGame() {
        this.game = null;
        for(const bottle of this.bottles){
            bottle.remove();
        }
        this.bottles = [];
    }

    newGame() {
        this.clearGame();
        this.game = new Logic();
        this.game.shuffle(100, 7);
        const b = this.game.viewAll();
        for(let i = 0; i < b.length; i++){
            const n = i;
            const bottleElm = document.createElement("div");
            this.base.appendChild(bottleElm);
            this.bottles.push(bottleElm);
            bottleElm.dataset.id = n + "";
            bottleElm.addEventListener("click", ()=>{this.triggerPour(n)})
            bottleElm.className = "bottle";
        }
        this.updateBottles();
        this.won = false;
    }

    updateBottles() {
        if(!this.game){
            return;
        }
        const b = this.game.viewAll()
        for(let i = 0; i < this.bottles.length; i++){
            //console.log(i, this.bottleGradient(b[i]));
            this.bottles[i].style.background = this.bottleGradient(b[i]);
        }
        this.hasWon();
    }

    hasWon() {
        if(!this.game){
            return;
        }
        if(this.game.hasWon()){
            this.base.style.background = "lightgreen";
            this.won = true;
        } else {
            this.base.style.background = "white";
            this.won = false;
        }
    }

    bottleGradient(colorNumbers: number[]) {
        const pct = 100 / colorNumbers.length;
        let css = "linear-gradient(0deg";
        for(let i = 0; i < colorNumbers.length; i++){
            css += `, ${colors[colorNumbers[i]]} ${pct * i}%, ${colors[colorNumbers[i]]} ${pct * (i + 1)}%`;
        }
        css += ")";
        return css;
    }

    triggerPour(n: number) {
        if(!this.game || this.won){
            return;
        }
        if(this.queued !== null){
            this.game.atob(this.queued, n);
            this.bottles[this.queued].style.borderColor = "";
            this.queued = null;
            this.updateBottles();
        } else {
            this.queued = n
            this.bottles[n].style.borderColor = "#e31111";
        }
    }
}