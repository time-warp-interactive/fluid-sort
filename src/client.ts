/// <reference lib="esNext" />
/// <reference lib="dom" />
/// <reference lib="webworker.importScripts" />
/// <reference lib="ScriptHost" />
/// <reference lib="dom.iterable" />
/// <reference no-default-lib="true"/>

import { GameGUI } from "./game.ts";

const game = new GameGUI(document.getElementById("content") as HTMLDivElement);

game.newGame();